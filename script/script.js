class Card{
    constructor(title, text, name, username, email, id){
        this.title    = title,
        this.text     = text,
        this.name     = name,
        this.username = username,
        this.email    = email,
        this.id       = id
    }
    render(){
        let card = `       
        <div class="card" id="${this.id}">
        <h4 class="title">${this.title}</h4>               
        <p class="user">                      
            <span class="name">${this.name}</span>        
            <span class="username">${this.username}</span>    
        </p>
        <p class="text">${this.text}</p>                  
        <p class="email">${this.email}</p> 
        <buton class="btn" id="${this.id}">delete</buton>
        </div>`
        return card
    }
}

async function users(){
    let usersFetch = await fetch("https://ajax.test-danit.com/api/json/users")
    let allUsers = await usersFetch.json()
    return allUsers
}

async function posts(){
    let postsFetch = await fetch("https://ajax.test-danit.com/api/json/posts")
    let allPosts = await postsFetch.json()
    return allPosts
}

async function loadAllDate(){
    let resalt = await Promise.all ([users(), posts()])
    return resalt
}

loadAllDate().then(([allUsers,allPosts]) =>{
    
    let conteiner = document.getElementById('conteiner')
    allPosts.forEach(post => {
        allUsers.forEach(user => {
            if(post.userId === user.id){
                let card = new Card (post.title, post.body, user.name, user.username, user.email, post.id)
                // console.log(card)
                conteiner.insertAdjacentHTML('beforeend', card.render())
            }
        })
    });

}) 

let conteiner = document.querySelector('.conteiner')
conteiner.addEventListener('click', () =>{
    if(event.target.id >0 && event.target.id <=100 ){
        let click = parent.event.target
        let num = click.id
        let div = document.getElementById(num)
        deletePost(num)
        conteiner.removeChild(div)
    }
})

function deletePost(arg){
    delete  fetch (`https://ajax.test-danit.com/api/json/posts/${arg}`)
}